SwayWM
######

:date: 2021-05-22 23:30
:modified: 2020-06-10 22:00
:tags: divers, desktop, wayland
:category: divers
:slug: sway
:authors: Milka64
:summary: À la découverte de **swayWM**, le clone de i3 sous wayland. En bonus pas mal d'outils bien utile...
:status: published

Sway, c'est quoi?
-----------------

**SwayWM** est un gestionnaire de fenêtre (sous *Wayland*) qui a pour but de remplacer **i3** (*x11*).

La configuration est même compatible de i3 vers sway.

Sway organise les fenêtres par division de l'espace en éléments de plus petites tailles. La première fenêtre ouverte *A* occupera tout l'espace. Lorsqu'une nouvelle fenêtre B est créée, elle prendra la moitié de l'espace (horizontalement ou verticalement) réduisant donc la taille de A de moitié. 

Si une nouvelle fenêtre C est créée, elle divisera encore l'espace d'une des deux autres fenêtres (selon le focus).

.. image:: {attach}images/sway/split.png
   :alt: split des fenêtres
   :target: {attach}images/sway/split.png

Sway permet également d'utiliser des *workspaces*, l'équivalent des **Bureaux** sous d'autres environnements (Gnome,KDE, etc), à la différence qu'ici un workspace utilise un écran et ne peut être étendu à d'autres écrans.


Wayland, le nouveau X11
-----------------------

Comme je le disait précédement, Sway utilise Wayland.

Il permet, entre autre, une meilleure sécurité que X11, un code plus moderne, mais moins mature.

Mais **tout n'est pas rose**, il y a quelques applications qui ne sont pas compatible (ou du moins en partie).

Mais les choses semblent aller de l'avant, exemple : **OBS-studio** a rendu, depuis peu, son logiciel compatbile Wayland.

Configuration
-------------

Tous se fait via le fichier de configuration ``~/.config/sway/config``. Il est extensible et peut donc être découpé au besoin.

La syntaxe est assez simple pour la plupart des usages et reste lisible.

J'ai personnellement choisi de versionner ce fichier dans mon **Gitlab**, ce qui me permet d'avoir la même configuration sur tous mes postes.

.. code-block:: bash
    
    set $mod Mod4
    
    xwayland enabled
    
    default_border normal
    default_floating_border normal
    set $opacity 0.9


Ici, je crée une variable ``$mod`` avec ``Mod4`` comme valeur.

``Mod4`` correspont à la touche Windows.

Ensuite, j'active Xwayland qui permet d'ouvrir des applications X11 sous wayland (en réalité, il ouvre un serveur X dans wayland)

Et je termine par la configuration des bordures et de l'opacité des fenêtres.

Ce fichier cherche à ouvrir ``config.local`` qui lui est spécifique au poste (Configuration éran, clavier, etc ...)


.. code-block:: bash

    include ~/.config/sway/config.local


Gestion des écrans
------------------

Le double écran se configure aussi explicitement que le reste.

.. code-block:: bash

    output HDMI1 pos 0 0 res 1920x1080
    output eDP1 pos 1920 0 res 1600x900

L'écran HDMI1 est à gauche, avec une résolution de 1920x1080 et le seconde écran (eDP1) est à droit avec une résolution de 1600x900.

Pour déterminer le nom des écran, il suffit d'utiliser la commande suivante.

.. code-block:: console
   
   $ swaymsg -t get_outputs

Workspaces
----------

Les workspaces sont donc l'équivalent des Bureaux sous **Gnome**/**KDE**/... on peut en configurer autant que l'on veut en leurs assignant simplement un raccourci clavier (voir plus bas).

Moi, je bascule d'un workspace à l'autre en utilisant la touche windows + le numéro du workspace (pour le workspace 4 : <Windows>+4)

Il y a également la possibilité de placer les workspaces sur un écran plutôt qu'un autre, et d'autres fonctions mais j'y reviendrais plus tard (via un MAJ de l'article).

Shortcuts : simplicité, efficacité
----------------------------------

Les raccourcis clavier se configurent avec ``bindsym``.

Ci-dessous, je configure mes raccourcis clavier pour les workspaces.

.. code-block:: bash

    # switch to workspace
    bindsym $mod+KP_1 workspace 1
    bindsym $mod+KP_2 workspace 2
    bindsym $mod+KP_3 workspace 3
    bindsym $mod+KP_4 workspace 4
    bindsym $mod+KP_5 workspace 5
    bindsym $mod+KP_6 workspace 6
    bindsym $mod+KP_7 workspace 7
    bindsym $mod+KP_8 workspace 8
    bindsym $mod+KP_9 workspace 9
    bindsym $mod+KP_0 workspace 10

**KP_1** correspond à la touche 1 du pavé numérique.

Mais bindsym peut également lancer des applications.

.. code-block:: bash

    # start a terminal
    bindsym $mod+t exec alacritty

Un autre de mes raccourcis que je trouve bien utile 

.. code-block:: bash

    # Reload the configuration file
    bindsym $mod+Shift+c reload

Sway peut utiliser des "modes". Lorsque l'un de ces modes est activé, les raccourci claviers sont uniques à ce mode.

Par exemple, moi j'utilise principalement trois modes:

* **default** : raccourcis claviers par défault
* **open** : il me permet d'ouvrir les applications que j'utilise souvent

    .. code-block:: bash
    
        mode "open" {
            bindsym e exec emoji-picker; mode "default"
            bindsym f exec firefox; mode "default"
            bindsym m exec thunderbird; mode "default"
            bindsym t exec alacritty; mode "default"
        
        
            # return to default mode
            bindsym Return mode "default"
            bindsym Escape mode "default"
        }
        bindsym $mod+o mode "open"


* **resize** : il me permet de redimensionner les fenêtres
    
    .. code-block:: bash

        mode "resize" {
            bindsym Left resize shrink width 10 px or 10 ppt
            bindsym Up resize grow height 10 px or 10 ppt
            bindsym Down resize shrink height 10 px or 10 ppt
            bindsym Right resize grow width 10 px or 10 ppt
        
            # return to default mode
            bindsym Return mode "default"
            bindsym Escape mode "default"
        }
        bindsym $mod+r mode "resize"


Autostart : rien de plus facile
-------------------------------

Les applications lancées à l'ouverture de la session se font tout simplement dans le même fichier que le reste de la configuration.

.. code-block:: bash

    exec mako
    exec /path/to/some/executable

Mes Dotfiles à moi
------------------

Vous pouvez retrouver la totalité de mes Dotfiles Ici.

.. code-block:: bash
    :linenos:

    include ~/.config/sway/config.local
    
    set $mod Mod4
    
    xwayland enabled
    
    default_border normal
    default_floating_border normal
    set $opacity 0.9
    
    #################
    #               #
    # windows rules #
    #               #
    #################
    
    for_window [class=".*"] border pixel 4
    for_window [title="."] border pixel 4
    
    for_window [class=".*"] opacity $opacity
    for_window [app_id=".*"] opacity $opacity
    for_window [floating=".*"] opacity 1
    
    for_window [app_id="firefox" title="^Incrustation vidéo$"] \
        floating enable, move position 477 450, sticky enable, opacity 1
    for_window [app_id="emoji-picker"] \
        floating enable, move position 477 450, sticky enable, opacity 1
    for_window [class="Steam" title="^Friends List$"] \
        floating enable, move position 1456, 43, sticky enable
    for_window [class="Steam" title="^Steam - News.*"] \
        floating enable, move position 1456, 43, sticky enable
    
    #################
    #               #
    #      Gaps     #
    #               #
    #################
    gaps inner 10
    gaps outer 4
    
    
    ##################
    #                #
    # windows colors #
    #                #
    ##################
    
    client.focused          #3a8b23 #3a8b23 #000000 #3a8b23   #3a8b23
    client.focused_inactive #94b78a #94b78a #000000 #94b78a   #94b78a
    client.unfocused        #505050 #505050 #000000 #ffffff   #505050
    client.urgent           #ffffff #ffffff #000000 #ffffff   #ffffff
    client.placeholder      #505050 #505050 #000000 #505050   #505050
    
    ###############
    #             #
    #  Wallpaper  #
    #             #
    ###############
    
    set $wallpapers_path $HOME/.config/wallpapers
    output * bg `find $wallpapers_path -type f | shuf -n 1` fill
    
    ##############
    #            #
    # autostart  #
    #            #
    ##############
    
    exec mako
    exec autotiling
    exec --no-startup-id xsettingsd &
    exec /opt/sway-alttab/target/release/sway-alttab
    exec alacritty
    
    #########################
    #                       #
    #  Let's play musique   #
    #                       #
    #########################
    
    exec mount.sshfs 0w.tf:/data/Downloads/Musique ~/Musique
    exec swayidle -w timeout 360 "~/.config/custom_scripts/swayloack.sh"
    exec mpd && mpc add / && mpc random on
    
    #####################
    #                   #
    #  Keyboard config  #
    #                   #
    #####################
    
    input "type:keyboard" {
        xkb_layout fr
        xkb_variant ,nodeadkeys
        xkb_numlock enabled
    }
    input * xkb_model "logitech_g15"
    
    ################
    #              #
    #  Workspaces  #
    #              #
    ################
    
    # Define names for default workspaces for which we configure key bindings later on.
    # We use variables to avoid repeating the names in multiple places.
    set $ws1 "1"
    set $ws2 "2"
    set $ws3 "3"
    set $ws4 "4"
    set $ws5 "5"
    set $ws6 "6"
    set $ws7 "7"
    set $ws8 "8"
    set $ws9 "9"
    set $ws10 "10"
    
    #######################
    #                     #
    #  keyboard shortcut  #
    #                     #
    #######################
    
    
    # Open generic soft with a key:
    
    mode "open" {
        bindsym e exec emoji-picker; mode "default"
        bindsym f exec firefox; mode "default"
        bindsym m exec thunderbird; mode "default"
        bindsym n exec alacritty --config-file ~/.config/alacritty/sd-network.yml; mode "default"
        bindsym t exec alacritty; mode "default"
    
    
        # return to default mode
        bindsym Return mode "default"
        bindsym Escape mode "default"
    }
    bindsym $mod+o mode "open"
    
    # Resizing containers:
    
    mode "resize" {
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Up resize grow height 10 px or 10 ppt
        bindsym Down resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt
    
        # return to default mode
        bindsym Return mode "default"
        bindsym Escape mode "default"
    }
    bindsym $mod+r mode "resize"
    
    bindsym $mod+KP_Subtract resize shrink width 10 px or 10 ppt;resize shrink height 10 px or 10 ppt
    bindsym $mod+KP_Add resize grow width 10 px or 10 ppt; resize grow height 10 px or 10 ppt
    
    # Exec cmd like gnome/mate
    bindsym Alt+F2 exec sgtk-dmenu -c -t 1 -w 700
    
    # Use Mouse+$mod to drag floating windows to their wanted position
    floating_modifier $mod
    
    # start a terminal
    bindsym $mod+t exec alacritty
    
    # kill focused window
    bindsym $mod+q kill
    
    # screenshot
    bindsym Print exec slurp | grim -g - $(xdg-user-dir PICTURES)/$(date +'screenshot_%Y-%m-%d-%H%M%S.png')
    
    # change focus
    bindsym $mod+Left focus left
    bindsym $mod+Down focus down
    bindsym $mod+Up focus up
    bindsym $mod+Right focus right
    
    # Move windows
    bindsym $mod+Shift+Left move left
    bindsym $mod+Shift+Down move down
    bindsym $mod+Shift+Up move up
    bindsym $mod+Shift+Right move right
    
    # Enter fullscreen mode for the focused container
    bindsym $mod+f fullscreen toggle
    
    # change container layout (stacked, tabbed, toggle split)
    #bindsym $mod+s layout stacking
    #bindsym $mod+w layout tabbed
    #bindsym $mod+e layout toggle split
    
    # toggle tiling / floating
    bindsym $mod+space floating toggle
    
    # focus the parent container
    bindsym $mod+a focus parent
    
    # focus the child container
    #bindsym $mod+d focus child
    
    
    # switch to workspace
    bindsym $mod+KP_1 workspace 1
    bindsym $mod+KP_2 workspace 2
    bindsym $mod+KP_3 workspace 3
    bindsym $mod+KP_4 workspace 4
    bindsym $mod+KP_5 workspace 5
    bindsym $mod+KP_6 workspace 6
    bindsym $mod+KP_7 workspace 7
    bindsym $mod+KP_8 workspace 8
    bindsym $mod+KP_9 workspace 9
    bindsym $mod+KP_0 workspace 10
    
    # switch to workspace
    bindsym $mod+ampersand workspace 1
    bindsym $mod+eacute workspace 2
    bindsym $mod+quotedbl workspace 3
    bindsym $mod+apostrophe workspace 4
    bindsym $mod+parenleft workspace 5
    bindsym $mod+minus workspace 6
    bindsym $mod+egrave workspace 7
    bindsym $mod+underscore workspace 8
    bindsym $mod+ccedilla workspace 9
    bindsym $mod+agrave workspace 10
    
    # move focused container to workspace
    bindsym $mod+Shift+ampersand move container to workspace 1; workspace 1
    bindsym $mod+Shift+eacute move container to workspace 2; workspace 2
    bindsym $mod+Shift+quotedbl move container to workspace 3; workspace 3
    bindsym $mod+Shift+apostrophe move container to workspace 4; workspace 4
    bindsym $mod+Shift+parenleft move container to workspace 5; workspace 5
    bindsym $mod+Shift+minus move container to workspace 6; workspace 6
    bindsym $mod+Shift+egrave move container to workspace 7; workspace 7
    bindsym $mod+Shift+underscore move container to workspace 8; workspace 8
    bindsym $mod+Shift+ccedilla move container to workspace 9; workspace 9
    bindsym $mod+Shift+agrave move container to workspace 10; workspace 10
    bindsym $mod+Shift+KP_1 move container to workspace 1 ;workspace 1
    bindsym $mod+Shift+KP_2 move container to workspace 2 ;workspace 2
    bindsym $mod+Shift+KP_3 move container to workspace 3 ;workspace 3
    bindsym $mod+Shift+KP_4 move container to workspace 4 ;workspace 4
    bindsym $mod+Shift+KP_5 move container to workspace 5 ;workspace 5
    bindsym $mod+Shift+KP_6 move container to workspace 6 ;workspace 6
    bindsym $mod+Shift+KP_7 move container to workspace 7 ;workspace 7
    bindsym $mod+Shift+KP_8 move container to workspace 8 ;workspace 8
    bindsym $mod+Shift+KP_9 move container to workspace 9 ;workspace 9
    bindsym $mod+Shift+KP_0 move container to workspace 10;workspace 10
    
    # volume control
    bindsym XF86AudioRaiseVolume exec "amixer set Master 5%+ unmute"
    bindsym XF86AudioLowerVolume exec "amixer set Master 5%- unmute"
    bindsym XF86AudioMute exec "amixer -q sset Master,0 toggle"
    
    # Brightness control
    bindsym XF86MonBrightnessDown exec brightnessctl set 15%-
    bindsym XF86MonBrightnessUp exec brightnessctl set +15%
    
    # Media player controls
    bindsym --locked XF86AudioPlay exec mpc toggle
    bindsym --locked XF86AudioPause exec mpc toggle
    bindsym --locked XF86AudioNext exec mpc next
    bindsym --locked XF86AudioPrev exec mpc prev
    bindsym --locked XF86AudioStop exec mpc stop
    
    # Reload the configuration file
    bindsym $mod+Shift+c reload
    
    ################
    #              #
    #  Font style  #
    #              #
    ################
    
    # Font for window titles. Will also be used by the bar unless a different font
    # is used in the bar {} block below.
    
    # This font is widely installed, provides lots of unicode glyphs, right-to-left
    # text rendering and scalability on retina/hidpi displays (thanks to pango).
    font pango:Hurmit NF 11
    
    set $gnome-schema org.gnome.desktop.interface
    exec_always {
        gsettings set $gnome-schema gtk-theme 'Material-Black-Lime-3.36'
        gsettings set $gnome-schema icon-theme 'Material-Black-Lime-Numix'
        gsettings set $gnome-schema font-name 'Hurmit NF 11'
    }
    
    
    ############
    #          #
    #  Waybar  #
    #          #
    ############
    
    bar {
        swaybar_command waybar
    }
    include ~/.config/sway/config.local

Tools
-----

J'utilise plein de petit "sous-programme" pour mon bureau, voici les plus utiles au quotidien.

Waybar : la barre à tout faire
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Pour mon bureau, j'utilise ``Waybar``. Il est comme Sway, il se configure très simplement par deux fichiers.

* ~/.config/waybar/config
* ~/.config/waybar/style.css

.. image:: {attach}images/sway/waybar.png
   :alt: waybar
   :target: {attach}images/sway/waybar.png

L'un permet de configurer les différents éléments de la barre et l'autre uniquement le style.

J'écrirai un autre article à ce sujet, il y a pas mal de choses sympa à dire.

gtk3-menu, c'est le pied!
~~~~~~~~~~~~~~~~~~~~~~~~~

gtk3-menu est juste un menu à la Gnome qui me permet d'ouvrir simplement quelques programmes dont je ne me souviens plus du nom via terminal. Il me permet également d'ouvrir rapidement un dossier présent dans mon $HOME.


.. image:: {attach}images/sway/gtk3-folder.png
   :alt: gtk3 folder menu
   :target: {attach}images/sway/gtk3-folder.png


L'écran de verrouillage : Swaylock
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

swaylock permet de verrouiller mon bureau afin que mes collègues ne s'en prennent pas à ma boite mail par exemple (merci les viennoiseries le lendemain ...).

Je vous conseille même l'excellent swaylock-effect qui permet de faire un screenshot, de le flouter et de l'avoir en fond d'écran.

.. image:: {attach}images/sway/swaylock.png
   :alt: swaylock
   :target: {attach}images/sway/swaylock.png

Capture écran
~~~~~~~~~~~~~

Pour la capture d'écran, j'utilise le combo slurp + grim.

Slurp permet de sélectionner une partie du bureau, et grim l'enregistre.

.. code-block:: bash

    # screenshot
    bindsym Print exec slurp | grim -g - $(xdg-user-dir PICTURES)/$(date +'screenshot_%Y-%m-%d-%H%M%S.png')

Mes capture d'écran se retrouvent automatiquement dans mon dossier Images avec date + heure dans le nom du fichier.

MPD/MPC
~~~~~~~

**MPD** (Music Player Daemon) est, comme son nom l'indique un lecteur de musique qui tourne en fond.

Il permet du coup d'être pilotable par de nombreux éléments externes comme **MPC** (Music Player Client).

Je l'utilise d'ailleurs directement dans ma waybar.

khal/vdirsyncer
~~~~~~~~~~~~~~~

Autre outil dans ma barre, le duo vdirsyncer + khal.

* vdirsyncer permet d'interroger un serveur DavCal et d'enregistrer une copie en ``.ics``
* khal permet lui de lire ces fichier ics directement en ligne de commande. C'est lui qui est directement interrogé depuis waybar.


