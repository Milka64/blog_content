Travailler avec un sysadmin aveugle, c'est comment?
###################################################

:date: 2021-07-01 08:00
:modified: 2021-07-01 08:00
:tags: divers
:category: divers
:slug: work_with
:authors: Milka64
:summary: Depuis quelque mois, j'ai un nouveau collegue hors du commun...
:status: draft

.. _Orca: https://wiki.gnome.org/Projects/Orca

Depuis quelques mois maintenant, j'ai un nouveau collegue qui a la particularité d'être aveugle.

La premiere question qui vient est evidement 

Mais comment fait-il pour travailler ?
--------------------------------------

La plupart du temps il utilise `Orca`, un lecteur d'écran. Ça lui permet de lire toute les informations qu'il y a à l'écran : les menus, les notifications, les contenus (page web, mail, ...). Il peut ainsi presque tout lire.

Mais pour lire dans les logs, ou quand il y a beaucoup de texte, il utilise un afficheur braille. Et comme son nom l'indique, c'est un outil qui lui retranscrit tous les caractères sous forme de braille.

Lors de son premier entretien, il s'est deja fait remarquer car il était le seul a avoir un nom configuré sur sa ligne SIP (Perso pour le coup), alors que nous n'utilisions que nos numéro cours.




ideas
-----

* moment drole (écran éteint, console noir sur noir, allo orca...)
* moment compliqué (schéma, pages web avec beaucoup d'éléments ou JS)
* point bloquant (graphs,)
* sa vision de l'info (orca, afficheur braille, etc ...)

plan
----

* le debut, la rencontre (parler du cimpte SIP) et les premiers pas
* la routine qui s'installe : les pieges dejoué et les limites a connaitre
* les points toujours bloquant, conclusion sur le fait d'essayer de trouver/coder des outils en cli ou conterner les obstacles
