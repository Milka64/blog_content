Introduction
############

:date: 2020-10-25 15:20
:modified: 2020-10-25 15:20
:tags: divers
:category: divers
:slug: introduction
:authors: Milka64
:summary: Présentation du blog: qui suis-je? où vais-je? dans quelle étagère?
:status: published

.. _Amstrad CPC464: https://fr.wikipedia.org/wiki/Amstrad_CPC_464
.. _Lilo: https://fr.wikipedia.org/wiki/Lilo

Moi
---

Je suis quelqu'un de curieux, de passionné, autodidacte et amoureux de l'opensource. J'aime me simplifier la vie en automatisant tout ce qui peut l'être.

Aussi longtemps que je me souvienne, j'ai toujours été attiré par la tech (au sens large). J'ai fait mes premiers pas sur un `Amstrad CPC464`_. J'y lançais des jeux stockés sur une cassette audio, on pouvait les dupliquer rien qu'avec une chaine hi-fi. La belle époque ...

J'ai gardé cette bête une bonne dizaine d'années et j'ai migré sur un Pentium II, sous windows 98 SE ! C'est à ce moment la ou j'ai commencé à m'interresser aux composants interne, à l'évolution d'un PC et à bidouiller mon OS.

Puis, au fil des années, j'ai peu à peu entendu parler de l'open source et de Linux. 

J'ai dans un premier temps testé un liveCD qui était conseillé par un site de news (genre Clubic/NextInpact). Et, petit à petit, j'ai appris à partitionner mon disque, gérer le multiboot (`Lilo`_ a l'époque), les différents bureaux disponibles (pouvoir choisir son bureau, je ne comprends pas pourquoi Microsoft ne s'y met toujours pas ...), etc ...

J'ai donc débarqué dans le monde du travail avec une première expérience perso d'un OS alternatif et j'ai très vite installé un linux sur HDD externe. Ça a été très formateur de devoir trouver des alternatives à différents logiciels proprietaires et métiers. J'y ai également découvert mes premières lignes de code et j'ai tout de suite vu l'intérêt de pouvoir scripter tout un tas de choses.

J'ai, quelques temps après ma première embauche, intégré le LUG local et découvert tout type de profil (que je ne pensais pas découvrir dans une association de ce genre).

Pourquoi
--------

Alors pourquoi un blog alors que ce n'est plus du tout "à la mode"? Pourquoi pas une chaine youtube, twitch ou d'autres plateformes plus récentes? Tout simplement parce que je trouve ce format plus simple a tenir à jour, un éditeur de texte suffit et je peux même ecrire depuis mon smartphone si ça me chante.

C'est aussi un très bon exercice de rédaction. Je suis souvent ammené à écrire de la documentation, des compte rendu, etc ... Je me suis dis qu'un blog pouvais m'aider à progresser.

J'ai pris le parti de l'écrire en français. Non pas par chauvisme ou autre, mais je trouve que les débutants que je côtoie ont du mal à trouver des infos dans la jungle des articles anglophones (ce qui n'exclue pas que je traduise certains articles).

Et enfin la dernière raision , c'est que j'ai toujours aimé pouvoir partager mon expérience, ce qui a fonctionné et ce qui a raté. Pouvoir échanger, sur les avis, les optionions.


Comment
-------

Ce blog utilise principalement deux outils :

- Git : pour le versionning des articles
- Pelican : pour le rendu html

Lorsque je push dans git, le site est reconstrui automatiquement avec les dernirères modiications.

C'est simple, efficace, pas besoin de se casser la tete avec une sauvegarde d'une quelconque base de données, les articles sont sur de simple fichier texte.

Le tout est auto-heberger sur un serveur d'occasion que j'ai sauvé de la destruction.

Quoi
----

Alors, de quoi vais-je bien pouvoir parler ici ? Il y a bien des thèmes qui viennent en tête et voici une liste non-exhaustive :

- réseau
- un peu de code (& pas de cul, on n'est pas sur sam&max)
- administration système
- sécurité
- de l'électronique/domotique
- et j'en oublie certainement plein d'autres

