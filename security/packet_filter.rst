Packet filter
#############

:date: 2021-05-18 07:35
:modified: 2021-05-18 07:35
:tags: bsd, securité
:category: securité
:slug: packet-filter
:authors: Milka64
:summary: Pourquoi j'aime tant packet filter
:status: draft


Présentation
------------

Packet filter est un pare-feu intégré au noyau BSD. Il apparait pour la première foir dans la version 3.0 d'OpenBSD (1 December 2001).

Écrit par Daniel Hartmeier, il a été ensuite réimlplémenté par Ryan McBride et Henning Brauer, qui est toujours le mainteneur principal.


Configuration
-------------

.. code-block:: shell

    # grep "pf_" /etc/rc.conf
    pf_enable=yes to /etc/rc.conf
    pf_rules="/path/to/pf.conf"


* syntaxe
* exemple de fichier de conf
* variable
* nat
* auto block
