Gérer son agenda en cli
#######################

:date: 2021-06-20 18:30
:modified: 2021-06-29 22:30
:tags: divers, cli
:category: cli
:slug: agenda_cli
:authors: Milka64
:summary: vdirsyncer & khal, le duo gagnant !
:status: published

Si il y a bien un truc que j'aime c'est de pouvoir **TOUT** gérer via le terminal. De pouvoir m'affranchir si besoin de toute interface graphique. De rester indépendant de tout problème d'affichage.

Et dans ce lot d'utilitaire accessible en console, j'ai récemment découvert le duo vdirsyncer & khal.

Vdirsyncer
----------

Vdirsyncer permet de synchroniser un calendrier en ligne (caldav, google calendar, etesync, etc...) avec un dossier local. Il permet également de synchroniser des contacts avec un serveur cardav, mais je ne développerais pas cette partie.

Le fichier de configuration se trouve ici :

.. code-block:: bash

    .config/vdirsyncer/config

Tout d'abord, configurons le status_path

.. code-block:: ini

    [general]
    status_path = "~/.vdirsyncer/status/"

Ensuite, nous pouvons configurer les deux calendriers à synchroniser.

L'un local et l'autre distant.

.. code-block:: ini

    [storage local]
    type = "filesystem"
    path = "~/.calendars/"
    fileext = ".ics"
    
    [storage remote]
    type = "caldav"
    url = "https://caldav.server/
    username = "foo"
    password = "bar"

Maintenant que nous avons configuré les calendriers, nous pouvons enfin configurer la synchronisation entre les deux calendriers. Cette étape parait idiote, mais elle est indispensable parce que l'outil permet de synchroniser plusieurs agendas.

.. code-block:: ini

    [pair caldavserver]
    a = "local"
    b = "remote"
    collections = ["from a", "from b"]
    metadata = ["color"]


Il ne reste plus qu'à synchroniser tout ça.

.. code-block:: console

    $ vdirsyncer discover
    $ vdirsyncer sync

Khal
----

Maintenant que nouqs avons Vdirsyncer qui stocke les agendas dans le dossier ``.calendars``, nous pouvons demander a khal de nous afficher les évenements.

Mais un minimum de configuration est necessaire

.. code-block:: ini

    [calendars]
    
    [[local]]
    path = ~/.calendars/*
    type = discover
    
    [locale]
    timeformat = %H:%M
    dateformat = %d/%m/%Y
    longdateformat = %d/%m/%Y
    datetimeformat = %d/%m/%Y %H:%M
    longdatetimeformat = %d/%m/%Y %H:%M
    
    [default]
    default_calendar = personal

Rien de particulier à dire sur cette configuration qui me parait plutôt claire.

.. code-block:: console

    $ khal list
    No events
    $ khal search dentiste
    02/07/2021 10:30-02/07/2021 11:30 Dentiste Dr Brutal
    $ khal --help
    Usage: khal [OPTIONS] COMMAND [ARGS]...

    Options:
      -v, --verbosity LVL    Either CRITICAL, ERROR,
                             WARNING, INFO or DEBUG
      -l, --logfile LOGFILE  The logfile to use [defaults
                             to stdout]
      -c, --config PATH      The config file to use.
      --color / --no-color   Use colored/uncolored output.
                             Default is to only enable
                             colors when not part of a
                             pipe.
      --version              Show the version and exit.
      --help                 Show this message and exit.
    
    Commands:
      at              Print all events at a specific
                      datetime...
      calendar        Print calendar with agenda.
      configure       Helper for initial configuration of
                      khal.
      edit            Interactively edit (or delete)
                      events...
      import          Import events from an .ics file (or
                      stdin).
      interactive     Interactive UI.
      list            List all events between a start
                      (default:...
      new             Create a new event from arguments.
      printcalendars  List all calendars.
      printformats    Print a date in all formats.
      printics        Print an ics file (or read from
                      stdin)...
      search          Search for events matching
                      SEARCH_STRING.
