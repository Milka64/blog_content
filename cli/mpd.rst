MPD
###

:date: 2021-06-08 23:30
:modified: 2021-06-08 23:30
:tags: divers, cli, multimedia
:category: cli
:slug: mpd
:authors: Milka64
:summary: **MPD** est un lecteur de musique qui tourne en daemon.
:status: published

Présentation
------------

MPD (Music Player Daemon) est, comme son nom l'indique, est un daemon qui joue de la musique.

Écrit en C, il est publié sous license GPLv2.

Fonctionement
-------------

MPD utilise une base de donnée fichier (comme sqlite3) pour stocker les informations de base des fichier audio. Le path de cette base est bien évidement configurable.

Il lit les fichiers en local et peut-être controlé par un client (local ou via réseau).

Configuration
-------------

La configuration est très simple

.. code-block:: kconfig

    music_directory "~/Musique"
    playlist_directory "~/Playlists"
    db_file "~/.mpd.db"
    log_file "syslog"
    state_file "/var/lib/mpd/mpdstate"
    auto_update	"yes"


Et c'est tout ? 

Et bien oui. On peut y ajouter tout plein d'options, de plugins, etc ... Mais l'essentiel est là.

Et maintenant?
--------------

Et bien maintenant, nous pouvons lancer le démon de façon très simple

.. code-block:: console

    $ mpd


et y connecter un client, on va commencer par utiliser mpc.

.. code-block:: console

    $ mpc update
    Updating DB (#1) ...
    volume: n/a   repeat: off   random: on    single: off   consume: off
    $ mpc add /
    $ mpc play
    Artist_name - Song_name
    [playing] #167/1427   0:00/4:32 (0%)
    volume: n/a   repeat: off   random: on    single: off   consume: off
    $ mpc next
    Artist_name - Song_name
    [playing] #414/1427   0:00/3:22 (0%)
    volume: 99%  repeat: off   random: on    single: off   consume: off
    $ mpc stop
    volume: 99%   repeat: off   random: on    single: off   consume: off


Les commandes parlent d'elle même, on update la DB, on ajoute toute la librairie musicale, on play, passe à la suivante et enfin on STOP.

