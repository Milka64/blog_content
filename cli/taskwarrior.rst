Taskwarrior
###########

:date: 2021-06-10 22:15
:modified: 2021-06-10 22:15
:tags: divers, cli
:category: cli
:slug: taskwarrior
:authors: Milka64
:summary: **Taskwarrior**, le meilleur gestionnaire de TODOlist de tout les temps!
:status: published

Un petit article pour vous faire découvrir un logiciel en ligne de commande.

Si il y a bien un outil que j'affectionne depuis plusieurs années, c'est Taskwarrior.

Ce logiciel permet de gérer tout type de tâche. De la tâche la plus simple à la tache hebdomadaire mais seulement pendant 6 mois.

Exemple d'utilisation
---------------------

Ajout d'une tâche et sa suppression.

.. code-block:: shell

    $ task add do something
    Created task 1.

    $ task list
    
    ID Age Description      Urg
     1 35s do something        0
    
    1 task
    $ task 1 done
    Completed task 1 'do something'.
    Completed 1 task.


On peut éditer une tâche via son éditeur préféré (variable $EDITOR)

.. code-block:: vim

    $ task edit 2
    # Name               Editable details
    # -----------------  ----------------------------------------------------
    # ID:                2
    # UUID:              24935a08-7577-4190-adc4-dc535382045b
    # Status:            Pending
    # Mask:
    # iMask:
      Project:
    # Separate the tags with spaces, like this: tag1 tag2
      Tags:
      Description:       do another thing
      Created:           2021-06-10 22:15:09
      Started:
      Ended:
      Scheduled:
      Due:
      Until:
      Recur:
      Wait until:
    # Modified:          2021-06-10 22:15:09
      Parent:
    # Annotations look like this: <date> -- <text> and there can be any number of them.
    # The ' -- ' separator between the date and text field should not be removed.
    # A "blank slot" for adding an annotation follows for your convenience.
      Annotation:        2021-06-10 22:15:26 --
    # Dependencies should be a comma-separated list of task IDs/UUIDs or ID ranges, with no spaces.
      Dependencies:
    # User Defined Attributes
      UDA priority:
    # End

Tout un tat de fonctionnalité sont disponibles:

* échéance
* dépendances entre tâches
* priorité
* classement par projets
* tags
* possibilité de cacher une tache jusqu'a une certaine date

Pour plus d'infos, je vous recommande l'excellent article de Linux Pratique
https://connect.ed-diamond.com/Linux-Pratique/LP-072/Taskwarrior-rien-que-pour-vos-taches

