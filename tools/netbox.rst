Netbox
######

:date: 2020-11-08 19:52
:modified: 2020-11-08 19:52
:tags: divers, tools
:category: tools
:slug: netbox
:authors: Milka64
:summary: Le logiciel libre de gestion de datacenter, écrit en python, il utilise le framework Django.
:status: draft

Introduction
------------

Netbox est un outil de gestion de parc informatique (principalement tout ce qui se trouve en datacenter).

Il permet entre autre de gerer :

* Lieux (dc1, dc2, etc...)
* Salles
* Racks
* équipements
* IPs
* VLANs
* interfaces
* virtualisation
* cluster de serveurs
* types de matériels
* consomation électrique
* câbles, mais vraiment tout type de câbles : alim, réseau, console, etc ...
* ...

Le tout peut être etendu grace à des plugins.

.. image:: {attach}images/netbox/start.png
   :alt: page d'accueil de netbox
   :target: {attach}images/netbox/start.png

Gestion des racks
-----------------

Le premier tableau des rack montre déjà un des gros avantage de cet outil.

En un seul tableau on le principal : 

* Le site
* Le status
* La taille (oui parfois ça compte)
* le taux d'occupation
* Le taux de consommation électrique (théorique)

La vue du rack est très utile, nous pouvons très vite voir le type d'équipement présent sur le rack grace au code de couleur.

En plus de tout cela, on peu ajouter des images, des équipements non racké, et on a même la possibilité de faire des groupes de rack.

Le petit bonus que j'aime beaucoup, c'est la possibilité de réserver une partie d'un rack (nouveau projet/client)


Gestion des équipements
-----------------------

Montrer les images dans l'équipement, les modeles, les marques, les composants ...

Plugins
-------

plugins sympa (je ne sais plus les noms ....)

Avis sur le soft
----------------

Manque la possibilité de faire un cluster de 4 noeud.
